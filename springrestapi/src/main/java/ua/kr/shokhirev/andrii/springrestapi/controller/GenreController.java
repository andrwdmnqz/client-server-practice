package ua.kr.shokhirev.andrii.springrestapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.kr.shokhirev.andrii.springrestapi.model.dto.AuthorRespDTO;
import ua.kr.shokhirev.andrii.springrestapi.model.dto.GenreRespDTO;
import ua.kr.shokhirev.andrii.springrestapi.service.GenreService;

import java.util.List;

@RestController
@RequestMapping("/genres")
public class GenreController {

    @Autowired
    private GenreService genreService;

    @GetMapping
    public ResponseEntity<List<GenreRespDTO>> getGenres() {

        List<GenreRespDTO> genreList;
        try {
            genreList = genreService.getAll();
            if (genreList.isEmpty()) {
                throw new RuntimeException();
            }
        } catch (RuntimeException e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<GenreRespDTO>>(genreList, HttpStatus.OK);
    }
}
