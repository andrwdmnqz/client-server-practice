package ua.kr.shokhirev.andrii.springrestapi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.kr.shokhirev.andrii.springrestapi.model.Author;
import ua.kr.shokhirev.andrii.springrestapi.model.dto.AuthorRespDTO;
import ua.kr.shokhirev.andrii.springrestapi.model.mappers.AuthorMapper;
import ua.kr.shokhirev.andrii.springrestapi.repository.AuthorRepository;

import java.util.List;
import java.util.Optional;

@Service
public class AuthorService {

    @Autowired
    private AuthorRepository authorRepository;

    @Autowired
    private AuthorMapper authorMapper;

    public List<AuthorRespDTO> getAll() {
        return authorRepository.findAll().stream().map(authorMapper::getAuthorResponseFromAuthor).toList();
    }

    public Optional<AuthorRespDTO> getSingleAuthor(Long id) {
        return Optional.ofNullable(authorMapper.getAuthorResponseFromAuthor(authorRepository.findById(id).get()));
    }
}
