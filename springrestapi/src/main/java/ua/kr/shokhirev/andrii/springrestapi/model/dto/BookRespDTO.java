package ua.kr.shokhirev.andrii.springrestapi.model.dto;

import lombok.Builder;
import lombok.Data;

import java.util.Set;

@Data
@Builder
public class BookRespDTO {

    private Long id;

    private String name;

    private Set<GenreRespDTO> genres;

    private AuthorRespDTO author;

    private Long publicationYear;
}
