package ua.kr.shokhirev.andrii.springrestapi.model.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;

import java.util.Set;

@Data
@Builder
public class BookReqDTO {

    @NotBlank(message = "Name should not be blank or null!")
    private String name;

    @NotNull(message = "Genre should not be null!")
    private Set<Long> genres;

    @NotNull(message = "Publication year should not be null!")
    private Long publicationYear;

    @NotNull(message = "Author id should not be null!")
    private Long authorId;
}
