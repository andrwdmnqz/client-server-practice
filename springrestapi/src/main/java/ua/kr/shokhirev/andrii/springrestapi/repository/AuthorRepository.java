package ua.kr.shokhirev.andrii.springrestapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.kr.shokhirev.andrii.springrestapi.model.Author;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Long> {

}
