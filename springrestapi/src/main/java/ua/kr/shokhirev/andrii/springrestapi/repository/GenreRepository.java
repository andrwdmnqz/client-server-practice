package ua.kr.shokhirev.andrii.springrestapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.kr.shokhirev.andrii.springrestapi.model.Genre;

@Repository
public interface GenreRepository extends JpaRepository<Genre, Long> {

}
