package ua.kr.shokhirev.andrii.springrestapi.model.mappers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.kr.shokhirev.andrii.springrestapi.model.Book;
import ua.kr.shokhirev.andrii.springrestapi.model.dto.BookReqDTO;
import ua.kr.shokhirev.andrii.springrestapi.model.dto.BookRespDTO;
import ua.kr.shokhirev.andrii.springrestapi.service.AuthorService;
import ua.kr.shokhirev.andrii.springrestapi.service.GenreService;

import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class BookMapper {

    @Autowired
    private AuthorService authorService;

    @Autowired
    private AuthorMapper authorMapper;

    @Autowired
    private GenreMapper genreMapper;

    @Autowired
    private GenreService genreService;

    public Book getBookFromRequest(BookReqDTO bookReqDTO) {
        return Optional.ofNullable(Book.builder()
                    .name(bookReqDTO.getName())
                    .genres(bookReqDTO.getGenres().stream().map(e -> genreMapper.genreResponseToGenre(
                            genreService.getSingleGenre(e).get())).collect(Collectors.toSet()))
                    .author(authorMapper.authorResponseToAuthor(authorService.getSingleAuthor(bookReqDTO.getAuthorId()).get()))
                    .publicationYear(bookReqDTO.getPublicationYear())
                    .build())
                .orElse(null);
    }

    public BookRespDTO bookToBookResponse(Book book) {
        if (book == null) {
            return null;
        }
        return Optional.ofNullable(BookRespDTO.builder()
                    .id(book.getId())
                    .name(book.getName())
                    .genres(book.getGenres().stream().map(e -> genreMapper.getGenreResponseFromGenre(e)).collect(Collectors.toSet()))
                    .author(authorMapper.getAuthorResponseFromAuthor(book.getAuthor()))
                    .publicationYear(book.getPublicationYear())
                    .build())
                .orElse(null);
    }
}
