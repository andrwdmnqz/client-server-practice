package ua.kr.shokhirev.andrii.server;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import ua.kr.shokhirev.andrii.clients.Client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

import static junit.framework.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

public class ServerThreadTest {

    @Mock
    private Socket socket;
    @Mock
    private BufferedReader mockBufferedReader;
    @Mock
    private PrintWriter mockPrintWriter;
    @Mock
    private String nickname;

    private ServerThread serverThread;

    @BeforeEach
    public void setup() {
        socket = mock(Socket.class);
        mockPrintWriter = mock(PrintWriter.class);
        mockBufferedReader = mock(BufferedReader.class);

        serverThread = new ServerThread(socket);
        serverThread.setBufferedReader(mockBufferedReader);
        serverThread.setPrintWriter(mockPrintWriter);
    }

    @AfterEach
    public void shutdown() {
        serverThread.shutdownThread();
    }

    @Test
    public void setPrintWriterTest() {
        PrintWriter printWriter = mock(PrintWriter.class);
        serverThread.setPrintWriter(printWriter);

        assertEquals(printWriter, serverThread.getPrintWriter());
    }

    @Test
    public void setNullPrintWriterTest() {

        IllegalArgumentException illegalArgumentException = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            serverThread.setPrintWriter(null);
        });

        Assertions.assertEquals(IllegalArgumentException.class, illegalArgumentException.getClass());
    }

    @Test
    public void setBufferedReaderTest() {
        BufferedReader bufferedReader = mock(BufferedReader.class);
        serverThread.setBufferedReader(bufferedReader);

        assertEquals(bufferedReader, serverThread.getBufferedReader());
    }

    @Test
    public void setNullBufferedReaderTest() {

        IllegalArgumentException illegalArgumentException = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            serverThread.setBufferedReader(null);
        });

        Assertions.assertEquals(IllegalArgumentException.class, illegalArgumentException.getClass());
    }

    @Test
    public void setNicknameTest() {
        String nickname = "test nickname";
        serverThread.setNickname(nickname);

        assertEquals(nickname, serverThread.getNickname());
    }

    @Test
    public void setNullNicknameTest() {
        IllegalArgumentException illegalArgumentException = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            serverThread.setNickname(null);
        });

        Assertions.assertEquals(IllegalArgumentException.class, illegalArgumentException.getClass());
    }

    @Test
    public void shutdownTest() throws IOException {

        serverThread.shutdownThread();

        verify(mockPrintWriter, times(1)).close();
        verify(mockBufferedReader, times(1)).close();
        verify(socket, times(1)).isClosed();
        verify(socket, times(1)).close();
    }

    @Test
    public void testShutdownIOException() throws IOException {

        Socket mockSocket = mock(Socket.class);
        ServerThread serverThread = new ServerThread(mockSocket);
        BufferedReader mockBufferedReader = mock(BufferedReader.class);
        PrintWriter mockPrintWriter = mock(PrintWriter.class);
        serverThread.setBufferedReader(mockBufferedReader);
        serverThread.setPrintWriter(mockPrintWriter);

        IOException exception = new IOException("Error closing resource");
        doThrow(exception).when(mockBufferedReader).close();

        IOException ioException = assertThrows(IOException.class, () -> serverThread.shutdownThread());
        Assertions.assertEquals("Error closing resource", ioException.getMessage());
    }

    @Test
    public void sendMessageTest() {
        String messageToSend = "Message to send";

        serverThread.sendMessage(messageToSend);

        verify(mockPrintWriter, times(1)).println(messageToSend);
    }

    @Test
    public void broadcastTest() {
        serverThread.setNickname("test nickname");
        Server.serverThreads.add(serverThread);
        String messageToBroadcast = "Message to broadcast";

        serverThread.broadcast(messageToBroadcast);
    }

    @Test
    public void runTest() throws IOException {
        when(mockBufferedReader.readLine()).thenThrow(new RuntimeException());

        RuntimeException runtimeException = Assertions.assertThrows(RuntimeException.class, () -> {
            serverThread.run();
        });

        verify(mockPrintWriter, times(1)).close();

        Assertions.assertEquals(RuntimeException.class, runtimeException.getClass());
    }
}
