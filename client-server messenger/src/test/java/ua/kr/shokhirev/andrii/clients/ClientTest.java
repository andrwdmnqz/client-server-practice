package ua.kr.shokhirev.andrii.clients;

import org.junit.jupiter.api.*;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.kr.shokhirev.andrii.server.Server;
import ua.kr.shokhirev.andrii.server.ServerThread;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@Component
public class ClientTest {

    private static final int TEST_PORT = 8888;
    private static final String TEST_HOST = "localhost";

    @Mock
    private Socket mockSocket;
    @Mock
    private BufferedReader mockBufferedReader;
    @Mock
    private PrintWriter mockPrintWriter;

    private Client client;

    @BeforeEach
    public void setup() {
        client = new Client();
        mockSocket = mock(Socket.class);
        mockBufferedReader = mock(BufferedReader.class);
        mockPrintWriter = mock(PrintWriter.class);

        client.setClientSocket(mockSocket);
        client.setBufferedReader(mockBufferedReader);
        client.setPrintWriter(mockPrintWriter);
    }

    @AfterEach
    public void cleanup() throws IOException {
        client.shutdown();
    }

    @Test
    public void getSocketTest() {
        Socket compareSocket = mockSocket;
        assertEquals(compareSocket, client.getClientSocket());
    }

    @Test
    public void setSocketTest() {
        Socket socket = new Socket();

        client.setClientSocket(socket);

        assertEquals(socket, client.getClientSocket());
    }

    @Test
    public void setNullSocketTest() {

        Socket nullSocket = null;

        IllegalArgumentException nullSocketExpection =Assertions.assertThrows(IllegalArgumentException.class, () -> {
            client.setClientSocket(nullSocket);
        });

        assertEquals(IllegalArgumentException.class, nullSocketExpection.getClass());
    }

    @Test
    public void setNullWriterTest() {

        PrintWriter nullWriter = null;

        IllegalArgumentException nullSocketExpection = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            client.setPrintWriter(nullWriter);
        });

        assertEquals(IllegalArgumentException.class, nullSocketExpection.getClass());
    }

    @Test
    public void setNullReaderTest() {

        BufferedReader bufferedReader = null;

        IllegalArgumentException nullSocketException = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            client.setBufferedReader(bufferedReader);
        });

        assertEquals(IllegalArgumentException.class, nullSocketException.getClass());
    }

    @Test
    public void runTest() throws IOException {
        client.run();

        verify(mockBufferedReader, times(1)).close();
        verify(mockPrintWriter, times(1)).close();
        verify(mockSocket, times(1)).isClosed();
        verify(mockSocket, times(1)).close();
    }

    @Test
    public void shutdownTest() throws IOException {

        client.shutdown();

        // Assert
        verify(mockBufferedReader, times(1)).close();
        verify(mockPrintWriter, times(1)).close();
        verify(mockSocket, times(1)).isClosed();
        verify(mockSocket, times(1)).close();
    }

    @Test
    public void testShutdownIOException() throws IOException {

        Client client = new Client();
        Socket mockSocket = mock(Socket.class);
        BufferedReader mockBufferedReader = mock(BufferedReader.class);
        PrintWriter mockPrintWriter = mock(PrintWriter.class);
        client.setClientSocket(mockSocket);
        client.setBufferedReader(mockBufferedReader);
        client.setPrintWriter(mockPrintWriter);

        IOException exception = new IOException("Error closing resource");
        doThrow(exception).when(mockBufferedReader).close();

        IOException ioException = assertThrows(IOException.class, () -> client.shutdown());
        assertEquals("Error closing resource", ioException.getMessage());
    }
}
