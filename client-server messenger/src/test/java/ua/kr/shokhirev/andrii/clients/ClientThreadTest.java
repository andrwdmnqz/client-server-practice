package ua.kr.shokhirev.andrii.clients;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class ClientThreadTest {

    @Mock
    private PrintWriter mockPrintWriter;
    @Mock
    private BufferedReader mockBufferedReader;

    private ClientThread clientThread;

    @BeforeEach
    public void setup() {
        mockPrintWriter = mock(PrintWriter.class);
        mockBufferedReader = mock(BufferedReader.class);

        clientThread = new ClientThread(mockPrintWriter);

        clientThread.setPrintWriter(mockPrintWriter);
        clientThread.setBufferedReader(mockBufferedReader);
    }

    @AfterEach
    public void shutdown() {
        clientThread.shutdown();
    }

    @Test
    public void setPrintWriterTest() {
        PrintWriter printWriter = mock(PrintWriter.class);
        clientThread.setPrintWriter(printWriter);

        assertEquals(printWriter, clientThread.getPrintWriter());
    }

    @Test
    public void setNullPrintWriterTest() {

        IllegalArgumentException illegalArgumentException = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            clientThread.setPrintWriter(null);
        });

        Assertions.assertEquals(IllegalArgumentException.class, illegalArgumentException.getClass());
    }

    @Test
    public void setBufferedReaderTest() {
        BufferedReader bufferedReader = mock(BufferedReader.class);
        clientThread.setBufferedReader(bufferedReader);

        assertEquals(bufferedReader, clientThread.getBufferedReader());
    }

    @Test
    public void setNullBufferedReaderTest() {

        IllegalArgumentException illegalArgumentException = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            clientThread.setBufferedReader(null);
        });

        Assertions.assertEquals(IllegalArgumentException.class, illegalArgumentException.getClass());
    }

    @Test
    public void shutdownTest() {
        clientThread.shutdown();

        verify(mockPrintWriter, times(1)).close();
    }

    @Test
    public void runTest() throws IOException {
        when(mockBufferedReader.readLine()).thenThrow(new RuntimeException());

        RuntimeException runtimeException = Assertions.assertThrows(RuntimeException.class, () -> {
            clientThread.run();
        });

        verify(mockPrintWriter, times(1)).close();

        Assertions.assertEquals(RuntimeException.class, runtimeException.getClass());
    }
}
