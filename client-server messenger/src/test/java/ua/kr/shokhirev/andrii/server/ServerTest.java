package ua.kr.shokhirev.andrii.server;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class ServerTest {

    private Server server;

    private ServerSocket mockServerSocket;

    @BeforeEach
    public void setup() {
        server = new Server();
        mockServerSocket = mock(ServerSocket.class);

        server.setServerSocket(mockServerSocket);
    }

    @AfterEach
    public void shutdownServer() {
        server.shutdown();
    }

    @Test
    public void shutdownTest() throws IOException {

        server.shutdown();
    }

    @Test
    public void shutdownTestIOException() throws IOException {
        Socket socket = mock(Socket.class);
        ServerThread serverThread = mock(ServerThread.class);
        when(mockServerSocket.isClosed()).thenThrow(new RuntimeException());

        Server.serverThreads.add(serverThread);

        server.shutdown();
    }

    @Test
    public void setNullServerSocket() {

        ServerSocket nullServerSocket = null;

        IllegalArgumentException nullSocketException = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            server.setServerSocket(nullServerSocket);
        });

        assertEquals(IllegalArgumentException.class, nullSocketException.getClass());
    }

//    @Test
//    public void testRun() throws IOException {
//
//        when(mockServerSocket.isClosed()).thenThrow(new RuntimeException());
//
//        IOException ioException = Assertions.assertThrows(IOException.class, () -> {
//            server.run();
//        });
//
//        assertEquals(IOException.class, ioException.getClass());
//    }
}
