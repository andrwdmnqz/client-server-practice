package ua.kr.shokhirev.andrii.server;

import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

@Component
public class ServerThread implements Runnable {

    private Socket clientSocket;
    private BufferedReader bufferedReader;
    private PrintWriter printWriter;
    private String nickname;
    private String timeStamp;

    public ServerThread (Socket socket) {
        this.clientSocket = socket;
    }

    @Override
    public void run() {
        try {
            printWriter = new PrintWriter(clientSocket.getOutputStream(), true);
            bufferedReader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            printWriter.println("Please enter a nickname: ");
            nickname = bufferedReader.readLine();

            SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date());

            System.out.println(nickname + " connected at " + timeStamp + "!");

            broadcast(nickname + " joined to the chat!");

            String message;

            while ((message = bufferedReader.readLine()) != null) {

                if (message.startsWith("/nick ")) {
                    String[] newNickname = message.split(" ", 2);
                    if (newNickname.length == 2) {
                        broadcast(nickname + " renamed himself to " + newNickname[1]);
                        nickname = newNickname[1];
                        printWriter.println("Successfully changed nickname to " + nickname);
                    } else {
                        printWriter.println("No nickname given!");
                    }

                } else if (message.startsWith("close")) {
                    String disconnectTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date());
                    System.out.println(nickname + " disconnected at " + disconnectTime + "!");
                    broadcast(nickname + " lived the chat!");
                    shutdownThread();

                } else broadcast(nickname + ": " + message);
            }

        } catch (IOException e) {
            shutdownThread();
        }
    }

    public void sendMessage(String message) {
        printWriter.println(message);
    }

    public void broadcast(String message) {
        ArrayList<ServerThread> serverThreadArrayList = Server.serverThreads;
        for (ServerThread serverThread: serverThreadArrayList) {
            if (serverThread != null && !this.nickname.equals(serverThread.nickname)) {
                serverThread.sendMessage(message);
            }
        }
    }

    public void shutdownThread() {
        try {
            printWriter.close();
            bufferedReader.close();
            if (!clientSocket.isClosed()) {
                clientSocket.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setPrintWriter(PrintWriter printWriter) {
        if (printWriter != null) {
            this.printWriter = printWriter;
        } else throw new IllegalArgumentException("Print writer cannot be null!");
    }

    public PrintWriter getPrintWriter() {
        return printWriter;
    }

    public void setBufferedReader(BufferedReader bufferedReader) {
        if (bufferedReader != null) {
            this.bufferedReader = bufferedReader;
        } else throw new IllegalArgumentException("Buffered reader cannot be null!");
    }

    public BufferedReader getBufferedReader() {
        return bufferedReader;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        if (nickname != null && !nickname.equals("")) {
            this.nickname = nickname;
        } else throw new IllegalArgumentException("Nickname cannot be null or empty!");
    }
}