package ua.kr.shokhirev.andrii.server;

import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

@Component
public class Server implements Runnable {

    public static ArrayList<ServerThread> serverThreads = new ArrayList<>();
    private ServerSocket serverSocket;

    public Server() {

    }

    @Override
    public void run() {

        try {
            serverSocket = new ServerSocket(9999);

            while (!serverSocket.isClosed()) {
                Socket clientSocket = serverSocket.accept();
                ServerThread serverThread = new ServerThread(clientSocket);
                serverThreads.add(serverThread);
                Thread t = new Thread(serverThread);
                t.start();
            }

        } catch (Exception e) {
            shutdown();
        }
    }

    public void shutdown() {
        try {
            if (serverSocket != null) {
                if (!serverSocket.isClosed()) {
                    serverSocket.close();
                }
                for (ServerThread serverThread: serverThreads) {
                    serverThread.shutdownThread();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Server server = new Server();
        server.run();
    }

    public void setServerSocket(ServerSocket serverSocket) {
        if (serverSocket != null) {
            this.serverSocket = serverSocket;
        } else throw new IllegalArgumentException("Server socket cannot be null!");
    }
}