package ua.kr.shokhirev.andrii.clients;

import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

@Component
public class ClientThread implements Runnable {

    private PrintWriter printWriter;
    private BufferedReader inReader;

    public ClientThread(PrintWriter printWriter) {
        this.printWriter = printWriter;
    }

    @Override
    public void run() {
        try {
            inReader = new BufferedReader(new InputStreamReader(System.in));

            while (true) {
                String message = inReader.readLine();

                if (message.equals("close")) {
                    printWriter.println(message);
                    inReader.close();
                    shutdown();
                } else {
                    printWriter.println(message);
                }

            }

        } catch (Exception e) {
            shutdown();
        }
    }

    public void shutdown() {
        printWriter.close();
    }

    public void setPrintWriter(PrintWriter printWriter) {
        if (printWriter != null) {
            this.printWriter = printWriter;
        } else throw new IllegalArgumentException("Print writer cannot be null!");
    }

    public PrintWriter getPrintWriter() {
        return printWriter;
    }

    public void setBufferedReader(BufferedReader bufferedReader) {
        if (bufferedReader != null) {
            this.inReader = bufferedReader;
        } else throw new IllegalArgumentException("Buffered reader cannot be null!");
    }

    public BufferedReader getBufferedReader() {
        return inReader;
    }
}