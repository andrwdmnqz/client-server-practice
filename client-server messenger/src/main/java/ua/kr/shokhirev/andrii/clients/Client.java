package ua.kr.shokhirev.andrii.clients;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.kr.shokhirev.andrii.server.ServerThread;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Client implements Runnable {

    private Socket clientSocket;
    private BufferedReader bufferedReader;
    private PrintWriter printWriter;

    @Override
    public void run() {

        try {
            clientSocket = new Socket("localhost", 9999);
            bufferedReader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            printWriter = new PrintWriter(clientSocket.getOutputStream(), true);

            ClientThread clientThread = new ClientThread(printWriter);
            Thread thread = new Thread(clientThread);
            thread.start();

            String inMessage;

            while ((inMessage = bufferedReader.readLine()) != null) {
                System.out.println(inMessage);
            }

        } catch (IOException e) {
            try {
                shutdown();
            } catch (IOException ex) {

            }
        }
    }

    public void shutdown() throws IOException {
        IOException exception = null;

        try {
            if (bufferedReader != null) {
                bufferedReader.close();
            }
        } catch (IOException e) {
            exception = e;
        }

        if (printWriter != null) {
            printWriter.close();
        }

        try {
            if (clientSocket != null && !clientSocket.isClosed()) {
                clientSocket.close();
            }
        } catch (IOException e) {
            if (exception == null) {
                exception = e;
            }
        }

        if (exception != null) {
            throw exception;
        }
    }

    public static void main(String[] args) {
        Client client = new Client();
        client.run();
    }

    public Socket getClientSocket() {
        return clientSocket;
    }

    public void setClientSocket(Socket clientSocket) {
        if (clientSocket != null) {
            this.clientSocket = clientSocket;
        } else throw new IllegalArgumentException("Socket cannot be null!");
    }

    public void setBufferedReader(BufferedReader bufferedReader) {
        if (bufferedReader != null) {
            this.bufferedReader = bufferedReader;
        } else throw new IllegalArgumentException("Buffered reader cannot be null!");
    }

    public void setPrintWriter(PrintWriter printWriter) {
        if (printWriter != null) {
            this.printWriter = printWriter;
        } else throw new IllegalArgumentException("Print writer cannot be null!");
    }
}
